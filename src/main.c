
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <stdbool.h>
#define VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>

char const helpmsg[] =
	"Usage: vkinfo [OPTION]...\n"
	"Prints information about the current Vulkan implementation.\n\n"
	"Options:\n"
	"  -l, --long  Long listing, prints extensive information\n"
	"  -h          Print sizes of kiB, MiB, etc. instead of bytes\n"
	"  --help      Prints this help message and exits\n"
	"  --version   Prints the program version and exits\n";

char const version[] = "0.2.2";

void *lib;

static bool longlist = false;
static bool humanread = false;

static char const *hrsizes[] =
{
	"B",
	"kiB",
	"MiB",
	"GiB",
	"TiB",
};

PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr;
PFN_vkEnumerateInstanceExtensionProperties vkEnumerateInstanceExtensionProperties;
PFN_vkEnumerateInstanceLayerProperties vkEnumerateInstanceLayerProperties;
PFN_vkCreateInstance vkCreateInstance;
PFN_vkDestroyInstance vkDestroyInstance;
PFN_vkEnumeratePhysicalDevices vkEnumeratePhysicalDevices;
PFN_vkGetPhysicalDeviceProperties vkGetPhysicalDeviceProperties;
PFN_vkGetPhysicalDeviceQueueFamilyProperties vkGetPhysicalDeviceQueueFamilyProperties;
PFN_vkGetPhysicalDeviceMemoryProperties vkGetPhysicalDeviceMemoryProperties;

VkInstance inst;
uint32_t instver;

uint32_t physdevcnt;
VkPhysicalDevice *physdevs;
VkPhysicalDeviceProperties *physdevprops;
VkPhysicalDeviceMemoryProperties *physmemprops;

uint32_t *queuefamcnt;
VkQueueFamilyProperties **queuefamprops;

uint32_t instextcount;
VkExtensionProperties *instext;

uint32_t layercount;
VkLayerProperties *layerprops;
uint32_t *layerextcnt;
VkExtensionProperties **layerext;

static uint32_t apiversions[] =
{
	VK_MAKE_VERSION(1,1,0),
	VK_MAKE_VERSION(1,0,0)
};

static int loadlib(void)
{
	lib = dlopen("libvulkan.so", RTLD_LAZY);
	if (lib == NULL)
	{
		fprintf(stderr, "Vulkan driver is unavailable or unsupported\n");
		return 1;
	}

	return 0;
}

static PFN_vkVoidFunction loadfunc(VkInstance inst, char const *fn)
{
	PFN_vkVoidFunction func;

	if (vkGetInstanceProcAddr != NULL)
	{
		if ((func = vkGetInstanceProcAddr(inst, fn)) != NULL)
		{
			return func;
		}
	}

	if ((func = dlsym(lib, fn)) == NULL)
	{
		fprintf(stderr, "Error: Failed to load function %s\n", fn);
		exit(2);
	}
	return func;
}

static void getextensions(void)
{
	int i;
	VkResult err;

	vkEnumerateInstanceExtensionProperties	= (PFN_vkEnumerateInstanceExtensionProperties)loadfunc(NULL,
		"vkEnumerateInstanceExtensionProperties");
	vkEnumerateInstanceLayerProperties = (PFN_vkEnumerateInstanceLayerProperties)loadfunc(NULL, "vkEnumerateInstanceLayerProperties");

	err = vkEnumerateInstanceExtensionProperties(NULL, &instextcount, NULL);
	if (err < 0)
	{
		fprintf(stderr, "Error while gathering instance extensions; extensions cannot be shown\n");
		goto noinstext;
	}
	
	instext = malloc(sizeof(VkExtensionProperties) * instextcount);
	if (instext == NULL)
	{
		fprintf(stderr, "Error while gathering instance extensions; extensions cannot be shown\n");
		goto noinstext;
	}

	err = vkEnumerateInstanceExtensionProperties(NULL, &instextcount, instext);
	if (err < 0)
	{
		fprintf(stderr, "Error while gathering instance extensions; extensions cannot be shown\n");
		free(instext);
		instext = NULL;
	}

noinstext:
	err = vkEnumerateInstanceLayerProperties(&layercount, NULL);
	if (err < 0)
	{
		fprintf(stderr, "Error while gathering layers; layers cannot be shown\n");
		return;
	}
	
	layerprops = malloc(sizeof(VkLayerProperties) * layercount);
	if (layerprops == NULL)
	{
		fprintf(stderr, "Error while gathering layers; layers cannot be shown\n");
		return;
	}

	err = vkEnumerateInstanceLayerProperties(&layercount, layerprops);
	if (err < 0)
	{
		fprintf(stderr, "Error while gathering layers; layers cannot be shown\n");
		free(layerprops);
		layerprops = NULL;
		return;
	}

	layerextcnt = malloc(sizeof(uint32_t) * layercount);
	if (layerextcnt == NULL)
	{
		fprintf(stderr, "Error while gathering layers extensions; extensions cannot be shown\n");
		return;
	}

	layerext = malloc(sizeof(VkExtensionProperties *) * layercount);
	if (layerext == NULL)
	{
		fprintf(stderr, "Error while gathering layers extensions; extensions cannot be shown\n");
		free(layerextcnt);
		layerextcnt = NULL;
		return;
	}

	for (i = 0; i < (int)layercount; i++)
	{
		err = vkEnumerateInstanceExtensionProperties(layerprops[i].layerName, &layerextcnt[i], NULL);
		if (err < 0)
		{
			fprintf(stderr, "Error while gathering layers extensions; extensions cannot be shown\n");
			free(layerextcnt);
			layerextcnt = NULL;
			while (--i >= 0) free(layerext[i]);
			free(layerext);
			layerext = NULL;
			return;
		}
		
		layerext[i] = malloc(sizeof(VkExtensionProperties) * layerextcnt[i]);
		if (layerext[i] == NULL)
		{
			fprintf(stderr, "Error while gathering layers extensions; extensions cannot be shown\n");
			free(layerextcnt);
			layerextcnt = NULL;
			while (--i >= 0) free(layerext[i]);
			free(layerext);
			layerext = NULL;
			return;
		}

		err = vkEnumerateInstanceExtensionProperties(layerprops[i].layerName, &layerextcnt[i], layerext[i]);
		if (err < 0)
		{
			fprintf(stderr, "Error while gathering layers extensions; extensions cannot be shown\n");
			free(layerextcnt);
			layerextcnt = NULL;
			while (i >= 0) free(layerext[i]), i--;
			free(layerext);
			layerext = NULL;
			return;
		}
	}
}

static void createinst(void)
{
	size_t i;
	VkResult err;
	VkApplicationInfo app =
	{
		VK_STRUCTURE_TYPE_APPLICATION_INFO,
		NULL, "vkinfo", 0,
		NULL, 0,
		apiversions[0],
	};

	VkInstanceCreateInfo info =
	{
		VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		NULL, 0, &app,
		0, NULL, 0, NULL
	};

	vkCreateInstance = (PFN_vkCreateInstance)loadfunc(NULL, "vkCreateInstance");

	for (i = 0; i < sizeof(apiversions) / sizeof(apiversions[0]); i++)
	{
		app.apiVersion = apiversions[i];
		if ((err = vkCreateInstance(&info, NULL, &inst)) < 0)
		{
			inst = NULL;
			if (err == VK_ERROR_INCOMPATIBLE_DRIVER)
				continue;
			else
			{
				fprintf(stderr, "Error: Failed to create instance: ");
				switch (err)
				{
					case VK_ERROR_OUT_OF_HOST_MEMORY:
					case VK_ERROR_OUT_OF_DEVICE_MEMORY:
						fprintf(stderr, "out of memory\n");
						break;

					case VK_ERROR_INITIALIZATION_FAILED:
						fprintf(stderr, "init failed\n");
						break;

					default:
						fprintf(stderr, "unknown error\n");
						break;
				}
				exit(3);
			}
		}
		instver = app.apiVersion;
		break;
	}

	if (inst == NULL)
	{
		fprintf(stderr, "No matching vulkan version (driver broken)\n");
		exit(3);
	}
}

static void querydevices(void)
{
	int i;
	VkResult err;
	vkEnumeratePhysicalDevices = (PFN_vkEnumeratePhysicalDevices)loadfunc(inst, "vkEnumeratePhysicalDevices");
	vkGetPhysicalDeviceProperties = (PFN_vkGetPhysicalDeviceProperties)loadfunc(inst, "vkGetPhysicalDeviceProperties");
	vkGetPhysicalDeviceQueueFamilyProperties = (PFN_vkGetPhysicalDeviceQueueFamilyProperties)loadfunc(inst, "vkGetPhysicalDeviceQueueFamilyProperties");
	vkGetPhysicalDeviceMemoryProperties = (PFN_vkGetPhysicalDeviceMemoryProperties)loadfunc(inst, "vkGetPhysicalDeviceMemoryProperties");
	
	err = vkEnumeratePhysicalDevices(inst, &physdevcnt, NULL);
	if (err < 0)
	{
		fprintf(stderr, "Error: Failed to get device count: ");
		switch (err)
		{
			case VK_ERROR_OUT_OF_HOST_MEMORY:
			case VK_ERROR_OUT_OF_DEVICE_MEMORY:
				fprintf(stderr, "out of memory\n");
				break;
			
			case VK_ERROR_INITIALIZATION_FAILED:
				fprintf(stderr, "couldn't initialize devices\n");
				break;

			default:
				fprintf(stderr, "unknown error\n");
				break;
		}
		exit(4);
	}

	physdevs = malloc(sizeof(VkPhysicalDevice) * physdevcnt);
	if (physdevs == NULL)
	{
		fprintf(stderr, "Error: Failed to allocate memory for device info\n");
		exit(5);
	}

	err = vkEnumeratePhysicalDevices(inst, &physdevcnt, physdevs);
	if (err < 0)
	{
		fprintf(stderr, "Error: Failed to get devices: ");
		switch (err)
		{
			case VK_ERROR_OUT_OF_HOST_MEMORY:
			case VK_ERROR_OUT_OF_DEVICE_MEMORY:
				fprintf(stderr, "out of memory\n");
				break;
			
			case VK_ERROR_INITIALIZATION_FAILED:
				fprintf(stderr, "couldn't initialize devices\n");
				break;

			default:
				fprintf(stderr, "unknown error\n");
				break;
		}
		exit(4);
	}

	physdevprops = malloc(sizeof(VkPhysicalDeviceProperties) * physdevcnt);
	if (physdevprops == NULL)
	{
		fprintf(stderr, "Error: Failed to allocate memory for device info\n");
		exit(5);
	}

	physmemprops = malloc(sizeof(VkPhysicalDeviceMemoryProperties) * physdevcnt);
	if (physmemprops == NULL)
	{
		fprintf(stderr, "Error: Failed to allocate memory for device memory info\n");
		exit(5);
	}

	queuefamcnt = malloc(sizeof(uint32_t) * physdevcnt);
	if (queuefamcnt == NULL)
	{
		fprintf(stderr, "Error: Failed to allocate memory for queue family info\n");
		exit(5);
	}

	queuefamprops = malloc(sizeof(VkQueueFamilyProperties *) * physdevcnt);
	if (queuefamprops == NULL)
	{
		fprintf(stderr, "Error: Failed to allocate memory for queue family info\n");
		exit(5);
	}

	for (i = 0; i < (int)physdevcnt; i++)
	{
		vkGetPhysicalDeviceProperties(physdevs[i], &physdevprops[i]);
		vkGetPhysicalDeviceQueueFamilyProperties(physdevs[i], &queuefamcnt[i], NULL);
		queuefamprops[i] = malloc(sizeof(VkQueueFamilyProperties) * queuefamcnt[i]);
		if (queuefamprops[i] == NULL)
		{
			fprintf(stderr, "Error: Failed to allocate memory for queue family info\n");
			exit(5);
		}
		vkGetPhysicalDeviceQueueFamilyProperties(physdevs[i], &queuefamcnt[i], queuefamprops[i]);
		vkGetPhysicalDeviceMemoryProperties(physdevs[i], &physmemprops[i]);
	}

	free(physdevs);
}

static char const *getdevtype(VkPhysicalDeviceType type)
{
	switch (type)
	{
		case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "other";
		case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "integrated";
		case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "discrete";
		case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "virtual";
		case VK_PHYSICAL_DEVICE_TYPE_CPU: return "cpu";
		default: return "unknown";
	}
}

static char const *getvendor(uint32_t id)
{
	switch (id)
	{
		case 0x10de: return "nVidia";
		case 0x1002:
		case 0x1022: return "AMD";
		case 0x8086: return "Intel";
		default: return "unknown";
	}
}

static double printhr(uint64_t val, char const **size)
{
	int hrsize = 0;
	double out = (double)val;
	if (humanread)
	{
		while (out > 1024.0)
		{
			hrsize++;
			out /= 1024.0;
		}
		*size = hrsizes[hrsize];
		return out;
	}
	else
	{
		*size = "";
		return out;
	}
}

int main(int argc, char **argv)
{
	uint64_t totalmem;
	uint64_t devmem;
	int i;
	int j;
	int err;
	char const *hrsize = "";
	double size = 0;

	if ((err = loadlib()) != 0)
	{
		return err;
	}

	for (i = 1; i < argc; i++)
	{
		if (argv[i][0] == '-')
		{
			if (argv[i][1] == '-')
			{
				if (strcmp(argv[i], "--help") == 0)
				{
					fputs(helpmsg, stdout);
					return 0;
				}
				else if (strcmp(argv[i], "--version") == 0)
				{
					printf("vkinfo, ver. %s\n", version);
					fputs("Copyright (c) 2018  Gustaf Alhäll\n", stdout);
					return 0;
				}
				else if (strcmp(argv[i], "--long") == 0)
				{
					longlist = true;
				}
				else
				{
					printf("Error: Invalid argument %s\n", argv[i]);
					exit(1);
				}
				break;
			}
			else
			{
				for (j = 1; argv[i][j] != '\0'; j++)
				{
					switch (argv[i][j])
					{
						case 'l':
							longlist = true;
							break;

						case 'h':
							humanread = true;
							break;

						default:
							printf("Error: Invalid argument -%c\n", argv[i][j]);
							exit(1);
					}
				}
			}
		}
	}

	vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)loadfunc(NULL, "vkGetInstanceProcAddr");

	getextensions();
	createinst();

	querydevices();

	vkDestroyInstance = (PFN_vkDestroyInstance)loadfunc(inst, "vkDestroyInstance");
	vkDestroyInstance(inst, NULL);

	printf("Vulkan version: %i.%i\n",
		VK_VERSION_MAJOR(instver),
		VK_VERSION_MINOR(instver));
	if (instext != NULL)
	{
		for (i = 0; i < (int)instextcount; i++)
		{
			if (longlist)
			{
				printf("Instance extension:\n");
			  	printf("  Name: %s\n", instext[i].extensionName);
				printf("  Version: %i.%i.%i\n",
					VK_VERSION_MAJOR(instext[i].specVersion),
					VK_VERSION_MINOR(instext[i].specVersion),
					VK_VERSION_PATCH(instext[i].specVersion));
			}
			else
			{
				printf("Instance extension: %s %i.%i.%i\n", instext[i].extensionName,
					VK_VERSION_MAJOR(instext[i].specVersion),
					VK_VERSION_MINOR(instext[i].specVersion),
					VK_VERSION_PATCH(instext[i].specVersion));
			}
		}
	}

	if (layerprops != NULL)
	{
		for (i = 0; i < (int)layercount; i++)
		{
			if (longlist)
			{
				printf("Layer: \n");
			  	printf("  Name: %s\n", layerprops[i].layerName);
				printf("  Specification version: %i.%i.%i\n",
					VK_VERSION_MAJOR(layerprops[i].specVersion),
					VK_VERSION_MINOR(layerprops[i].specVersion),
					VK_VERSION_PATCH(layerprops[i].specVersion));
				printf("  Implementation version: %i.%i.%i\n",
					VK_VERSION_MAJOR(layerprops[i].implementationVersion),
					VK_VERSION_MINOR(layerprops[i].implementationVersion),
					VK_VERSION_PATCH(layerprops[i].implementationVersion));
				printf("  Description: %s\n", layerprops[i].description);
			}
			else
			{
				printf("Layer: %s %i.%i.%i %i.%i.%i %s\n", layerprops[i].layerName,
					VK_VERSION_MAJOR(layerprops[i].specVersion),
					VK_VERSION_MINOR(layerprops[i].specVersion),
					VK_VERSION_PATCH(layerprops[i].specVersion),
					VK_VERSION_MAJOR(layerprops[i].implementationVersion),
					VK_VERSION_MINOR(layerprops[i].implementationVersion),
					VK_VERSION_PATCH(layerprops[i].implementationVersion),
					layerprops[i].description);
			}

			if (layerext != NULL)
			{
				for (j = 0; j < (int)layerextcnt[i]; j++)
				{
					if (longlist)
					{
						printf("  Layer extension:\n");
						printf("    Name: %s\n", layerext[i][j].extensionName);
						printf("    Version: %i.%i.%i\n",
							VK_VERSION_MAJOR(layerext[i][j].specVersion),
							VK_VERSION_MINOR(layerext[i][j].specVersion),
							VK_VERSION_PATCH(layerext[i][j].specVersion));
					}
					else
					{
						printf("  Layer extension: %s %i.%i.%i\n", layerext[i][j].extensionName,
							VK_VERSION_MAJOR(layerext[i][j].specVersion),
							VK_VERSION_MINOR(layerext[i][j].specVersion),
							VK_VERSION_PATCH(layerext[i][j].specVersion));
					}
				}
			}
		}
	}

	for (i = 0; i < (int)physdevcnt; i++)
	{
		totalmem = 0;
		devmem = 0;
		for (j = 0; j < (int)physmemprops[i].memoryHeapCount; j++)
		{
			totalmem += physmemprops[i].memoryHeaps[j].size;
			if ((physmemprops[i].memoryHeaps[j].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) != 0)
			{
				devmem += physmemprops[i].memoryHeaps[j].size;
			}
		}

		if (longlist)
		{
			printf("Physical device:\n");
			printf("  API version: %i.%i.%i\n",
				VK_VERSION_MAJOR(physdevprops[i].apiVersion),
				VK_VERSION_MINOR(physdevprops[i].apiVersion),
				VK_VERSION_PATCH(physdevprops[i].apiVersion));
			printf("  Driver version: %i.%i.%i\n",
				VK_VERSION_MAJOR(physdevprops[i].driverVersion),
				VK_VERSION_MINOR(physdevprops[i].driverVersion),
				VK_VERSION_PATCH(physdevprops[i].driverVersion));
			printf("  Vendor ID: %.4x (%s)\n",
				physdevprops[i].vendorID,
				getvendor(physdevprops[i].vendorID));
			printf("  Device ID: %.4x (%s)\n",
				physdevprops[i].deviceID,
				physdevprops[i].deviceName);
			printf("  Device type: %s\n",
				getdevtype(physdevprops[i].deviceType));
			printf("  Pipeline cache UUID: %.2x%.2x%.2x%.2x-%.2x%.2x-%.2x%.2x-%.2x%.2x-%.2x%.2x%.2x%.2x%.2x%.2x\n",
				physdevprops[i].pipelineCacheUUID[0],
				physdevprops[i].pipelineCacheUUID[1],
				physdevprops[i].pipelineCacheUUID[2],
				physdevprops[i].pipelineCacheUUID[3],
				physdevprops[i].pipelineCacheUUID[4],
				physdevprops[i].pipelineCacheUUID[5],
				physdevprops[i].pipelineCacheUUID[6],
				physdevprops[i].pipelineCacheUUID[7],
				physdevprops[i].pipelineCacheUUID[8],
				physdevprops[i].pipelineCacheUUID[9],
				physdevprops[i].pipelineCacheUUID[10],
				physdevprops[i].pipelineCacheUUID[11],
				physdevprops[i].pipelineCacheUUID[12],
				physdevprops[i].pipelineCacheUUID[13],
				physdevprops[i].pipelineCacheUUID[14],
				physdevprops[i].pipelineCacheUUID[15]);
		}
		else
		{
			printf("Physical device: %i.%i.%i %i.%i.%i %.4x %s %.4x %s %s %.2x%.2x%.2x%.2x-%.2x%.2x-%.2x%.2x-%.2x%.2x-%.2x%.2x%.2x%.2x%.2x%.2x\n",
				VK_VERSION_MAJOR(physdevprops[i].apiVersion),
				VK_VERSION_MINOR(physdevprops[i].apiVersion),
				VK_VERSION_PATCH(physdevprops[i].apiVersion),
				VK_VERSION_MAJOR(physdevprops[i].driverVersion),
				VK_VERSION_MINOR(physdevprops[i].driverVersion),
				VK_VERSION_PATCH(physdevprops[i].driverVersion),
				physdevprops[i].vendorID,
				getvendor(physdevprops[i].vendorID),
				physdevprops[i].deviceID,
				physdevprops[i].deviceName,
				getdevtype(physdevprops[i].deviceType),
				physdevprops[i].pipelineCacheUUID[0],
				physdevprops[i].pipelineCacheUUID[1],
				physdevprops[i].pipelineCacheUUID[2],
				physdevprops[i].pipelineCacheUUID[3],
				physdevprops[i].pipelineCacheUUID[4],
				physdevprops[i].pipelineCacheUUID[5],
				physdevprops[i].pipelineCacheUUID[6],
				physdevprops[i].pipelineCacheUUID[7],
				physdevprops[i].pipelineCacheUUID[8],
				physdevprops[i].pipelineCacheUUID[9],
				physdevprops[i].pipelineCacheUUID[10],
				physdevprops[i].pipelineCacheUUID[11],
				physdevprops[i].pipelineCacheUUID[12],
				physdevprops[i].pipelineCacheUUID[13],
				physdevprops[i].pipelineCacheUUID[14],
				physdevprops[i].pipelineCacheUUID[15]);
		}

		if (humanread)
		{
			size = printhr(devmem, &hrsize);
			printf("  Device memory: %.2f %s\n", size, hrsize);
			size = printhr(totalmem, &hrsize);
			printf("  Total memory: %.2f %s\n", size, hrsize);
		}
		else
		{
			printf("  Device memory: %lu\n", devmem);
			printf("  Total memory: %lu\n", totalmem);
		}

		for (j = 0; j < (int)physmemprops[i].memoryTypeCount; j++)
		{
			if (longlist)
			{
				printf("  Memory type:\n");
				printf("    Heap index: %u\n", physmemprops[i].memoryTypes[j].heapIndex);
				printf("    Memory type: %s%s%s%s%s\n",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ? "D" : "-",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ? "V" : "-",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) ? "H" : "-",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) ? "C" : "-",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) ? "A" : "-");
			}
			else
			{
				printf("  Memory type: %u %s%s%s%s%s\n", physmemprops[i].memoryTypes[j].heapIndex,
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ? "D" : "-",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ? "V" : "-",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) ? "H" : "-",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) ? "C" : "-",
					(physmemprops[i].memoryTypes[j].propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) ? "A" : "-");
			}
		}

		for (j = 0; j < (int)physmemprops[i].memoryHeapCount; j++)
		{
			size = printhr(physmemprops[i].memoryHeaps[j].size, &hrsize);
			if (longlist)
			{
				printf("  Memory heap:\n");
				if (humanread)
					printf("    Size: %.2f %s\n", size, hrsize);
				else
					printf("    Size: %lu\n", physmemprops[i].memoryHeaps[j].size);

				printf("    Heap flags: %s\n",
					(physmemprops[i].memoryHeaps[j].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) ? "D" : "-");
			}
			else
			{
				if (humanread)
					printf("  Memory heap: %.2f %s %s\n", size, hrsize,
						(physmemprops[i].memoryHeaps[j].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) ? "D" : "-");
				else
					printf("  Memory heap: %lu %s\n", physmemprops[i].memoryHeaps[j].size,
						(physmemprops[i].memoryHeaps[j].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) ? "D" : "-");
			}
		}

		for (j = 0; j < (int)queuefamcnt[i]; j++)
		{
			if (longlist)
			{
				printf("  Queue family:\n");
				printf("    Queue flags: %s%s%s%s\n",
					(queuefamprops[i][j].queueFlags & VK_QUEUE_GRAPHICS_BIT) ? "G" : "-",
					(queuefamprops[i][j].queueFlags & VK_QUEUE_COMPUTE_BIT) ? "C" : "-",
					(queuefamprops[i][j].queueFlags & VK_QUEUE_TRANSFER_BIT) ? "T" : "-",
					(queuefamprops[i][j].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) ? "S" : "-");
				printf("    Queue count: %u\n", queuefamprops[i][j].queueCount);
				printf("    Timestamp-valid bits: %u\n", queuefamprops[i][j].timestampValidBits);
				printf("    Minimum image transfer granularity: {%u,%u,%u}\n",
					queuefamprops[i][j].minImageTransferGranularity.width,
					queuefamprops[i][j].minImageTransferGranularity.height,
					queuefamprops[i][j].minImageTransferGranularity.depth);
			}
			else
			{
				printf("  Queue family: %s%s%s%s %u %u {%u,%u,%u}\n",
					(queuefamprops[i][j].queueFlags & VK_QUEUE_GRAPHICS_BIT) ? "G" : "-",
					(queuefamprops[i][j].queueFlags & VK_QUEUE_COMPUTE_BIT) ? "C" : "-",
					(queuefamprops[i][j].queueFlags & VK_QUEUE_TRANSFER_BIT) ? "T" : "-",
					(queuefamprops[i][j].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) ? "S" : "-",
					queuefamprops[i][j].queueCount,
					queuefamprops[i][j].timestampValidBits,
					queuefamprops[i][j].minImageTransferGranularity.width,
					queuefamprops[i][j].minImageTransferGranularity.height,
					queuefamprops[i][j].minImageTransferGranularity.depth);
			}
		}
	}

	dlclose(lib);

	return 0;
}
