# vkinfo - Vulkan and GPU information and diagnostics tool

`vkinfo` is a tool that prints information about the current Vulkan
implementation. It can be used to diagnose problems with compatibility or
drivers.

## Installation

### Requirements

The following tools are required to build `vkinfo`:

 * git
 * gcc
 * make or scons
 * Vulkan headers

### Build

To build, clone the repo, `cd` into it and issue the following commands:

```
./configure
scons  # or make, if a makefile was generated
```

This will produce an executable named `vkinfo` in the project root directory.

### Install / Uninstall

There's no built-in way to install at the moment, but it can easily be installed
manually by copying `vkinfo` (After building) to `/usr/local/bin`. Uninstalling
simply requires removing `vkinfo` from `/usr/local/bin`.
